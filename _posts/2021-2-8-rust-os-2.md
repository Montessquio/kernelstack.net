---
layout: post
title:  "Rust OS: Exceptions and Memory Management"
excerpt: "A Complete Rust OS, Part 2"
date: 2021-2-8
tags: code project multi_part series_rust_os
published: true
---

In this post, we'll set up runtime error handling by catching CPU exceptions, and give ourselves access to the full arsenal of the `alloc` crate's functionality by implementing the paging we set up in the last post as well as implementing a heap allocator.

[Click here to see the full list of OSDev posts.](https://kernelstack.net/tags/#series_rust_os)

## Table of Contents <!-- omit in toc -->

- [Recommended Reading and Prerequisites](#recommended-reading-and-prerequisites)
- [Disclaimer](#disclaimer)
- [Handling Interrupts](#handling-interrupts)
  - [Set-Up](#set-up)
  - [Using the x86-64 crate](#using-the-x86-64-crate)
  - [Double Faults](#double-faults)
- [Global Descriptor Table](#global-descriptor-table)
  - [Stack Switching](#stack-switching)
  - [Enabling the GDT and TSS](#enabling-the-gdt-and-tss)
- [Source](#source)
- [Licensing](#licensing)

## Recommended Reading and Prerequisites

- You should have at least a basic understanding of CPU exceptions, paging, and memory management. If you don't, here are some links you can read that explain it better than I could:
  - Phillip Opperman's articles on the following subjects are great places to start:
    - [CPU Exceptions](https://os.phil-opp.com/cpu-exceptions/)
    - [Hardware Interrupts](https://os.phil-opp.com/hardware-interrupts/)
    - [Memory Paging](https://os.phil-opp.com/paging-introduction/)
    - [Heap Allocation](https://os.phil-opp.com/heap-allocation/)
  - The OSDev wiki's corresponding pages are also great places to get a more in-depth view of these concepts:
    - [Exceptions](https://wiki.osdev.org/Exceptions)
    - [Interrupts](https://wiki.osdev.org/Interrupts)
    - [Paging](https://wiki.osdev.org/Paging)
    - [Simple Heap Allocator Examples](https://wiki.osdev.org/User:Pancakes/SimpleHeapImplementation)
  - If you'd prefer to watch a video, you can see [this MIT lecture on Virtual Memory](https://youtube.com/watch?v=W7Scg6LfZhY)

## Disclaimer

Come of this section's code has been adapted both with and without modifications from [Philipp Oppermann's OSDev Blog](https://os.phil-opp.com/multiboot-kernel/){:target="_blank"}. For more information, see [Licensing](#licensing)

## Handling Interrupts

### Set-Up

Let's start this post by adding the `x86_64` crate as a dependency. This crate is conceptually similar to the `x86` crate we've already been using, which contains safe code for working with protected mode structures and instructions - this new crate does the same thing for long mode software.

```toml
[dependencies]
x86 = "^0.35.0"
x86_64 = "^0.13.2"
spin = "^0.7.1"

[dependencies.lazy_static]
version = "^1.4.0"
features = ["spin_no_std"]
```

Because interrupts are used for both CPU Exceptions *and* Peripheral I/O (which we will absolutely want to do in the future), we'll go ahead and preemptively create a new module to keep all our interrupt-related code in.

```rust
// src/lib.rs
#![feature(abi_x86_interrupt)] // Goes below `#[no_std]`

extern crate x86_64; // Goes below `extern crate x86;`

mod interrupts; // Goes below `mod serial;`
```

In our new module we can write out the boilerplate for creating an IDT and potentially registering handler functions to it. We wrap it in a `lazy_static!` right away because we know we want it to live the entire lifetime of the program, and not be dropped when the idt initialization function ends.

```rust
// src/interrupts.rs
use x86_64::structures::idt::InterruptDescriptorTable;
use lazy_static::lazy_static;

lazy_static! {
    static ref IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();
        // Set Handler Funcs below
        // idt.<interrupt>.set_handler_fn(<function>);

        idt
    };
}

pub fn init_idt() {
    IDT.load();
}
```

### Using the x86-64 crate
This [might seem like magic](https://os.phil-opp.com/cpu-exceptions/#too-much-magic), but there's not much going on under the crate lid, so to speak. The `x86_64` crate does three things for us here.

First, it saves us the trouble of writing out big data structures. The IDT boils down to the struct below, so why spend the trouble to write it out if it's just going to be identical to what the crate provides?

```rust
#[repr(C)]
pub struct InterruptDescriptorTable {
    pub divide_by_zero: Entry<HandlerFunc>,
    pub debug: Entry<HandlerFunc>,
    pub non_maskable_interrupt: Entry<HandlerFunc>,
    // Lots more of these
```

Second, it safely abstracts the handling of the actual CPU instructions used to load the new IDT into the CPU. This saves us the effort of writing several wrapper functions in rust just to perform a few instructions, thus keeping our own code cleaner and free of `unsafe`s.

Third, it takes care of figuring out the `x86-interrupt` calling convention's inputs, such as the `InterruptStackFrame` and `PageFaultErrorCode` for us, which are themselves provided by [an abstraction provided by Rust](https://doc.rust-lang.org/nomicon/ffi.html#foreign-calling-conventions) which can be used to write rust code that [can be used in the IDT](https://github.com/rust-lang/rust/issues/40180).

If you want to write your own IDT management code from scratch, you can refer to the links related to interrupts in the section [Recommended Reading and Prerequisites](#recommended-reading-and-prerequisites) as well as the [x86_64 crate source and documentation](https://docs.rs/x86_64/0.13.2/x86_64/).

### Double Faults

The double fault is one of the most pervasive exceptions an OS developer will encounter near the start of their project. Despite this, it's actually one of the easiest to catch. When a Double Fault is invoked, that simply means that the CPU encountered a different exception and fails to invoke its corresponding handler. If it then fails to invoke the double fault's handler, it causes a *triple fault* which has no handler and simply causes the CPU to reset itself.

Let's get started implementing a basic Double Fault handler. For now, it'll simply panic and log an error message and some relevant data to serial. Since it panics and therefore never returns, it will be marked as diverging (`-> !`).

> The `x86_64` crate implements `Debug` for the `InterruptStackFrame` struct, so we can effortlessly print the data within to Serial without much hassle.

```rust
// src/interrupts.rs
use x86_64::structures::idt::{InterruptDescriptorTable, InterruptStackFrame};
use lazy_static::lazy_static;

lazy_static! {
    static ref IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();
        // Set Handler Funcs below
        idt.double_fault.set_handler_fn(double_fault);

        idt
    };
}

pub fn init() {
    IDT.load();
}

extern "x86-interrupt" fn double_fault(
    stack_frame: &mut InterruptStackFrame, _error_code: u64) -> !
{
    panic!("EXCEPTION: DOUBLE FAULT\n{:#?}", stack_frame);
}
```

Now we can initialize the IDT and intentionally cause a page fault by writing to some unallocated memory. Since we have no page fault handler, our double fault handler should run. We shouldn't forget to upgrade our panic function while we're at it, since currently it simply hangs forever once called.

```rust
// src/lib.rs

#[no_mangle]
pub extern "C" fn rust_main() -> ! {
    printsln!("Hello, World!");

    interrupts::init();

    // trigger a page fault
    unsafe {
        *(0xdeadbeef as *mut u64) = 42;
    };

    loop {}
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    loop {}
}
```

One `make run` later and we can see that our `serial.log` reflects our changes beautifully! It even displays the location in the source where the panic occurred, thanks to rust's `PanicInfo` doing its magic.

![The Double Fault handler working as intended.](/img/osdev-double-fault-exception.png)

## Global Descriptor Table

In 32-bit protected mode, the [Global Descriptor Table](https://wiki.osdev.org/GDT_Tutorial) is used to manage segmentation. With the advent of 64-bit long mode and paging, this table has become obsolete in terms of memory management. However, we still need it in order to be able to switch between user mode and kernel mode, and to take advantage of some CPU capabilities which will keep our OS stable.

Let's start by writing a new module and taking advantage of the `x86_64` crate to define our GDT.

```rust
// src/lib.rs
mod gdt; // Goes below `mod interrupts;`

// _start function has changed
/// The OS long mode Rust entrypoint.
#[no_mangle]
pub extern "C" fn rust_main() -> ! {
    printsln!("Hello, World!");

    gdt::init(); // new
    interrupts::init();

    loop {}
}
```

Writing the GDT is a bit more complex than translating some boilerplate off of the OSDev wiki, since we want to make use of some functionality later to keep our kernel stable.

We'll start with some imports and a GDT structure, contained in a lazy static so it lives as long as the OS does. The GDT is a tuple, containing a `GlobalDescriptorTable` struct as well as a `Selectors` struct, which is defined below it.

```rust
use x86_64::structures::gdt::{GlobalDescriptorTable, Descriptor, SegmentSelector};

lazy_static! {
    static ref GDT: (GlobalDescriptorTable, Selectors) = {
        let mut gdt = GlobalDescriptorTable::new();
        let code_selector = gdt.add_entry(Descriptor::kernel_code_segment());
        (gdt, Selectors { code_selector })
    };
}
```

The GDT's construction is fairly simple on its own - we initialize the gdt, and add any selectors we want to it according to a struct we'll make next:

```rust
struct Selectors {
    code_selector: SegmentSelector,
}
```

The Selectors here allow us to select certain parts of the GDT that would otherwise be a pain to get later on. Now that they're stored in an accessible location, we can use them to reference which GDT and TSS 

```rust
pub fn init() {
    use x86_64::instructions::segmentation::set_cs;

    GDT.0.load();
    unsafe {
        set_cs(GDT.1.code_selector);
    }
}
```


### Stack Switching

If a stack overflow in the kernel leads to a double fault, then when the handler tries to push its own data onto the stack it will cause a triple fault. Fortunately, x86_64 CPUs are able to switch to a known-good stack whenever an exception handler is called, so this problem can never occur.

In order to implement this for the CPU, we need to make use of the [Task State Segment](https://wiki.osdev.org/Task_State_Segment). Not only will this allow us to fix our stack-sanity problems, but it will also come in handy for switching between user mode and kernel mode later on. Let's update our GDT module with a new TSS structure:

```rust
// src/gdt.rs

// new imports
use x86_64::VirtAddr;
use x86_64::structures::tss::TaskStateSegment;
use lazy_static::lazy_static;

pub const DOUBLE_FAULT_IST_INDEX: u16 = 0;

lazy_static! {
    static ref TSS: TaskStateSegment = {
        let mut tss = TaskStateSegment::new();
        tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX as usize] = {
            const STACK_SIZE: usize = 4096 * 5;
            static mut STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];

            let stack_start = VirtAddr::from_ptr(unsafe { &STACK });
            let stack_end = stack_start + STACK_SIZE;
            stack_end
        };
        tss
    };
}

// the rest of the GDT stuff goes below
```

Our TSS here is dense with information, so let's break it down bit by bit.

```rust
pub const DOUBLE_FAULT_IST_INDEX: u16 = 0;
// ...
tss.interrupt_stack_table[DOUBLE_FAULT_IST_INDEX as usize] = {
```

These two lines define which of the 7 free stacks in the TSS should be used by our double fault handler. In this case, it's the zeroth stack in the list.

```rust
const STACK_SIZE: usize = 4096 * 5;
static mut STACK: [u8; STACK_SIZE] = [0; STACK_SIZE];
```

The next set of two lines define the size of the clean stack we're allocating for the handler. We set it to be 5*4096 bytes because the size of each frame in memory will be 4kb. If this doesn't make sense to you yet, don't worry - we'll get to it later in this post.

```rust
let stack_start = VirtAddr::from_ptr(unsafe { &STACK });
let stack_end = stack_start + STACK_SIZE;
```

Our TSS doesn't store the stack itself, just a pointer to it. These two lines calculate the end of the stack (where new values would be placed) and stores it as that entry in the TSS.

Now that we've done this, we can simply add references to the TSS where needed:

```rust
// src/gdt.rs
lazy_static! {
    static ref GDT: (GlobalDescriptorTable, Selectors) = {
        let mut gdt = GlobalDescriptorTable::new();
        let code_selector = gdt.add_entry(Descriptor::kernel_code_segment());
        let tss_selector = gdt.add_entry(Descriptor::tss_segment(&TSS)); // new
        (gdt, Selectors { code_selector, tss_selector }) // updated
    };
}
struct Selectors {
    code_selector: SegmentSelector,
    tss_selector: SegmentSelector, // new
}

pub fn init() {
    use x86_64::instructions::segmentation::set_cs;
    use x86_64::instructions::tables::load_tss; // new

    GDT.0.load();
    unsafe {
        set_cs(GDT.1.code_selector);
        load_tss(GDT.1.tss_selector); // new
    }
}
```

### Enabling the GDT and TSS

By this point our kernel should be setting up the GDT and TSS just fine, and loading it into the correct register with no problems. But if we force another stack overflow, we'll find that it still triple faults. This is because our handler isn't set to use the clean stack we just reserved for it. Let's remedy that:

```rust
// src/interrupts.rs
use crate::gdt;

lazy_static! {
    static ref IDT: InterruptDescriptorTable = {
        let mut idt = InterruptDescriptorTable::new();
        // Set Handler Funcs below
        unsafe {
            idt.double_fault.set_handler_fn(double_fault)
                .set_stack_index(gdt::DOUBLE_FAULT_IST_INDEX);
        }
        idt
    };
}
```
The IDT's `set_handler_fn` function returns its own options, so we can set the stack index that way thanks to `x86_64`. It has no way of knowing that the stack index is good, so we use unsafe to indicate we know that the supplied index is good.

> Under the hood, the all the x86_64 crate does is some bitfield manipulation on the IDT's double fault handler entry to update which stack it's using. The operation boils down to a simple one-liner: `self.0.set_bits(0..3, index + 1)`.

Now if we run our intentionally overflowing kernel, we should see an overflow-related panic instead of a boot loop:



## Source

The entire source for this OS is available on the project's repository on GitHub: [https://github.com/Montessquio/Rust-OS/](https://github.com/Montessquio/Rust-OS/tree/Post-2).

## Licensing

This text includes software written by Philipp Oppermann and released under the MIT License, which is reproduced in full below:

The MIT License (MIT)

Copyright (c) 2015 Philipp Oppermann

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.