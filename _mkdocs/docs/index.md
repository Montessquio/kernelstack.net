# 3BINS
3BINS is an instruction set designed for use in execution environments where memory is abundant in comparison with the space given for instruction decoding and execution. It is with those types of constraints in mind that 3BINS contains only eight opcodes - hence the number three in the title - the opcode is contained in only three bits. 3BINS has no registers and operates entirely in memory.

## 3BINS Assembly
3BINS Assembly, or 3BA, assembles to the 3BINS instruction encoding. A 3BA assembler can be found at [link](link)