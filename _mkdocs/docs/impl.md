# Implementation and Instruction Encoding

3BINS instructions are fixed to 32 bits in length.

## Encoding Types

## 12 Bit Mode

In twelve bit mode, all addresses and immediates are twelve bits. Numerical and Binary arithmetic as well as comparison operations operate on 32-bit doublewords in memory.

- The three most significant bits contain the opcode.
- The next five bits comprise the opcode flags.
- The following twelve bits make up a pointer to the first operand in memory.
- The last twelve bits make up a pointer to the second operand in memory or an immediate value.

```binary
           12bit Arg1    12bit Arg2
          /           \ /           \
000 00000_00000000_0000 0000_00000000
\ / \   /
 |  Flags
 |
 Opcode
```

Operands work as pointers into memory, not values themselves (unless they are an immediate in an `INT` or `MMI` instruction).

```binary
Arg1 Value      Mem Addr    Mem Val
0xFFF       ->  0xFFF        0x23
```

> Note that because jumps use values stored in memory rather than immediate values, it is possible to construct a jump to high memory addresses of up to 32 bits in length.

This is the simplest mode and is best suited for tiny systems that require the absolutely minimal instruction set.

## Encodings and Flag Usage by Opcode

- MOV, opcode 0 (000)
  - When the least significant bit of flags is set (00001) it is interpreted as an MMI instruction, and therefore the second operand is treated as an immediate.
- ADD, opcode 1 (001)
- SUB, opcode 2 (010)
- OR, opcode 3 (011)
- NOT, opcode 4 (100)
- INT, opcode 5 (101)
  - All five bits of the flags field of this instruction are system-specific. Users should consult their implementation's manual for more information.
- CMP, opcode 6 (110)
- JMP, opcode 7 (111)
  - The lower three bits of the flags field indicate the jump condition.
  - A 0 in the flags field (000) indicates an unconditional jump.
  - A 1 in the flags field (001) indicates a jump if equal (JE)
  - A 2 in the flags field (010) indicates a jump if not equal (JNE)
  - A 3 in the flags field (010) indicates a jump if greater than (JG)
  - A 4 in the flags field (011) indicates a jump if less than (JL)
  - A 5 in the flags field (100) indicates a jump if overflow (JO)
  - A 6 in the flags field (101) indicates a jump if carry (JC)
  - The remaining two states do not represent anything and their behavior is undefined undefined.

The two most significant bits of the flags field always indicate the pointer mode of the instruction. In twelve bit mode, these are always zero.

## CMP Behavior

The CMP instruction compares the values stored in memory at the address pointed to by its operands and sets the appropriate flags in the state register at 0x4. CMPW, CMPD, and CMPQ instructions treat its two operands as 12-bit wide pointers to 16-bit, 32-bit, and 64-bit pointers to the values to be compared, respectively.

## Status Register Behavior

| Condition     | Bit Set in SR     |
|---------------|-------------------|
| Equal         | 10000000 00000000 |
| Less Than     | 01000000 00000000 |
| Greater Than  | 00100000 00000000 |
| Overflow      | 00010000 00000000 |
| Carry         | 00001000 00000000 |
| Stack Enabled | 00000100 00000000 |
| Error Dump    | 00000000 00000001 |

## 16 Bit Mode

Sixteen bit mode makes use of flags to allow instructions to treat its twelve bit operands as 12 bit wide pointers to 16 bit wide pointers to values, rather than pointers to values.

Before:

```binary
Arg1 Value      Mem Addr    Mem Val
0xFFF       ->  0xFFF       0x231
```

After:

```binary
Arg1 Value     Mem Addr     Mem Val     Mem Addr    Mem Val
0xFFF       -> 0xFFF         0x231    ->  0x231        0xDED
```

This allows a user to construct 16-bit wide pointers out of 12-bit values, and use said 16-bit pointers to access higher memory up to 0xFFFF (65535) bytes.

The most significant bit of the flags field is set (10000) to indicate that the instruction should use the 16-bit double-pointer mechanism.

## 16 Bit MMI

In 16 bit mode, an MMIW instruction takes different sizes of operands. The destination address is an eight-bit address in low memory, and the immediate value is a 16-bit immediate value. The immediate value will then be moved into the destination address.

## 32 Bit Mode

32 bit mode is just like 16 bit mode - it makes use of flags to allow instructions to treat its twelve bit operands as 12 bit wide pointers to 32 bit wide pointers to values, rather than pointers to values.

This allows a user to construct 32-bit wide pointers out of 12 or 16-bit values, and use said 32-bit pointers to access higher memory up to 0xFFFF_FFFF (4294967295) bytes.

The second most significant bit of the flags field is set (01000) to indicate that the instruction should use the 32-bit double-pointer mechanism.

## 32 Bit MMI

In 32 bit mode, an MMID instruction behaves like a 16-bit MMI instruction, except the 8-bit destination operand is treated as a pointer to a 32-bit pointer to high memory, where the 16-bit immediate value will be moved.

## 64 Bit Mode

64 bit mode is just like 16 and 32 bit modes - it makes use of flags to allow instructions to treat its twelve bit operands as 12 bit wide pointers to 64 bit wide pointers to values, rather than pointers to values.

This allows a user to construct 64-bit wide pointers out of 12 or 16-bit values, and use said 64-bit pointers to access higher memory up to 0xFFFFF_FFFF_FFFF_FFFF (18446744073709551615) bytes.

The two most significant bit of the flags field is set (11000) to indicate that the instruction should use the 64-bit double-pointer mechanism.

# 64 Bit MMI

In 64 bit mode, an MMIQ instruction behaves like a 16-bit MMI instruction, except the 8-bit destination operand is treated as a pointer to a 64-bit pointer to high memory, where the 16-bit immediate value will be moved.