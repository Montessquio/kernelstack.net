I"[F<p>In this post, we’ll set up runtime error handling by catching CPU exceptions, and give ourselves access to the full arsenal of the <code class="highlighter-rouge">alloc</code> crate’s functionality by implementing the paging we set up in the last post as well as implementing a heap allocator.</p>

<p><a href="https://kernelstack.net/tags/#series_rust_os">Click here to see the full list of OSDev posts.</a></p>

<h2 id="table-of-contents-">Table of Contents <!-- omit in toc --></h2>

<ul>
  <li><a href="#recommended-reading-and-prerequisites">Recommended Reading and Prerequisites</a></li>
  <li><a href="#disclaimer">Disclaimer</a></li>
  <li><a href="#handling-interrupts">Handling Interrupts</a>
    <ul>
      <li><a href="#set-up">Set-Up</a></li>
      <li><a href="#using-the-x86-64-crate">Using the x86-64 crate</a></li>
      <li><a href="#double-faults">Double Faults</a></li>
    </ul>
  </li>
  <li><a href="#global-descriptor-table">Global Descriptor Table</a>
    <ul>
      <li><a href="#stack-switching">Stack Switching</a></li>
    </ul>
  </li>
  <li><a href="#source">Source</a></li>
  <li><a href="#licensing">Licensing</a></li>
</ul>

<h2 id="recommended-reading-and-prerequisites">Recommended Reading and Prerequisites</h2>

<ul>
  <li>You should have at least a basic understanding of CPU exceptions, paging, and memory management. If you don’t, here are some links you can read that explain it better than I could:
    <ul>
      <li>Phillip Opperman’s articles on the following subjects are great places to start:
        <ul>
          <li><a href="https://os.phil-opp.com/cpu-exceptions/">CPU Exceptions</a></li>
          <li><a href="https://os.phil-opp.com/hardware-interrupts/">Hardware Interrupts</a></li>
          <li><a href="https://os.phil-opp.com/paging-introduction/">Memory Paging</a></li>
          <li><a href="https://os.phil-opp.com/heap-allocation/">Heap Allocation</a></li>
        </ul>
      </li>
      <li>The OSDev wiki’s corresponding pages are also great places to get a more in-depth view of these concepts:
        <ul>
          <li><a href="https://wiki.osdev.org/Exceptions">Exceptions</a></li>
          <li><a href="https://wiki.osdev.org/Interrupts">Interrupts</a></li>
          <li><a href="https://wiki.osdev.org/Paging">Paging</a></li>
          <li><a href="https://wiki.osdev.org/User:Pancakes/SimpleHeapImplementation">Simple Heap Allocator Examples</a></li>
        </ul>
      </li>
      <li>If you’d prefer to watch a video, you can see <a href="https://youtube.com/watch?v=W7Scg6LfZhY">this MIT lecture on Virtual Memory</a></li>
    </ul>
  </li>
</ul>

<h2 id="disclaimer">Disclaimer</h2>

<p>Come of this section’s code has been adapted both with and without modifications from <a href="https://os.phil-opp.com/multiboot-kernel/" target="_blank">Philipp Oppermann’s OSDev Blog</a>. For more information, see <a href="#licensing">Licensing</a></p>

<h2 id="handling-interrupts">Handling Interrupts</h2>

<h3 id="set-up">Set-Up</h3>

<p>Let’s start this post by adding the <code class="highlighter-rouge">x86_64</code> crate as a dependency. This crate is conceptually similar to the <code class="highlighter-rouge">x86</code> crate we’ve already been using, which contains safe code for working with protected mode structures and instructions - this new crate does the same thing for long mode software.</p>

<div class="language-toml highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="nn">[dependencies]</span>
<span class="py">x86</span> <span class="p">=</span> <span class="s">"^0.35.0"</span>
<span class="py">x86_64</span> <span class="p">=</span> <span class="s">"^0.13.2"</span>
<span class="py">spin</span> <span class="p">=</span> <span class="s">"^0.7.1"</span>

<span class="nn">[dependencies.lazy_static]</span>
<span class="py">version</span> <span class="p">=</span> <span class="s">"^1.4.0"</span>
<span class="py">features</span> <span class="p">=</span> <span class="nn">["spin_no_std"]</span>
</code></pre></div></div>

<p>Because interrupts are used for both CPU Exceptions <em>and</em> Peripheral I/O (which we will absolutely want to do in the future), we’ll go ahead and preemptively create a new module to keep all our interrupt-related code in.</p>

<div class="language-rust highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="c">// src/lib.rs</span>
<span class="nd">#![feature(abi_x86_interrupt)]</span> <span class="c">// Goes below `#[no_std]`</span>

<span class="k">extern</span> <span class="n">crate</span> <span class="n">x86_64</span><span class="p">;</span> <span class="c">// Goes below `extern crate x86;`</span>

<span class="k">mod</span> <span class="n">interrupts</span><span class="p">;</span> <span class="c">// Goes below `mod serial;`</span>
</code></pre></div></div>

<p>In our new module we can write out the boilerplate for creating an IDT and potentially registering handler functions to it. We wrap it in a <code class="highlighter-rouge">lazy_static!</code> right away because we know we want it to live the entire lifetime of the program, and not be dropped when the idt initialization function ends.</p>

<div class="language-rust highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="c">// src/interrupts.rs</span>
<span class="k">use</span> <span class="nn">x86_64</span><span class="p">::</span><span class="nn">structures</span><span class="p">::</span><span class="nn">idt</span><span class="p">::</span><span class="n">InterruptDescriptorTable</span><span class="p">;</span>
<span class="k">use</span> <span class="nn">lazy_static</span><span class="p">::</span><span class="n">lazy_static</span><span class="p">;</span>

<span class="nd">lazy_static!</span> <span class="p">{</span>
    <span class="k">static</span> <span class="k">ref</span> <span class="n">IDT</span><span class="p">:</span> <span class="n">InterruptDescriptorTable</span> <span class="o">=</span> <span class="p">{</span>
        <span class="k">let</span> <span class="k">mut</span> <span class="n">idt</span> <span class="o">=</span> <span class="nn">InterruptDescriptorTable</span><span class="p">::</span><span class="nf">new</span><span class="p">();</span>
        <span class="c">// Set Handler Funcs below</span>
        <span class="c">// idt.&lt;interrupt&gt;.set_handler_fn(&lt;function&gt;);</span>

        <span class="n">idt</span>
    <span class="p">};</span>
<span class="p">}</span>

<span class="k">pub</span> <span class="k">fn</span> <span class="nf">init_idt</span><span class="p">()</span> <span class="p">{</span>
    <span class="n">IDT</span><span class="nf">.load</span><span class="p">();</span>
<span class="p">}</span>
</code></pre></div></div>

<h3 id="using-the-x86-64-crate">Using the x86-64 crate</h3>
<p>This <a href="https://os.phil-opp.com/cpu-exceptions/#too-much-magic">might seem like magic</a>, but there’s not much going on under the crate lid, so to speak. The <code class="highlighter-rouge">x86_64</code> crate does three things for us here.</p>

<p>First, it saves us the trouble of writing out big data structures. The IDT boils down to the struct below, so why spend the trouble to write it out if it’s just going to be identical to what the crate provides?</p>

<div class="language-rust highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="nd">#[repr(C)]</span>
<span class="k">pub</span> <span class="k">struct</span> <span class="n">InterruptDescriptorTable</span> <span class="p">{</span>
    <span class="k">pub</span> <span class="n">divide_by_zero</span><span class="p">:</span> <span class="n">Entry</span><span class="o">&lt;</span><span class="n">HandlerFunc</span><span class="o">&gt;</span><span class="p">,</span>
    <span class="k">pub</span> <span class="n">debug</span><span class="p">:</span> <span class="n">Entry</span><span class="o">&lt;</span><span class="n">HandlerFunc</span><span class="o">&gt;</span><span class="p">,</span>
    <span class="k">pub</span> <span class="n">non_maskable_interrupt</span><span class="p">:</span> <span class="n">Entry</span><span class="o">&lt;</span><span class="n">HandlerFunc</span><span class="o">&gt;</span><span class="p">,</span>
    <span class="c">// Lots more of these</span>
</code></pre></div></div>

<p>Second, it safely abstract the handling of the actual CPU instructions used to load the new IDT into the CPU. This saves us the effort of writing several wrapper functions in rust just to perform a few instructions, thus keeping our own code cleaner.</p>

<p>Third, it takes care of figuring out the <code class="highlighter-rouge">x86-interrupt</code> calling convention’s inputs, such as the <code class="highlighter-rouge">InterruptStackFrame</code> and <code class="highlighter-rouge">PageFaultErrorCode</code> for us, which are themselves provided by <a href="https://doc.rust-lang.org/nomicon/ffi.html#foreign-calling-conventions">an abstraction provided by Rust</a> which can be used to write rust code that <a href="https://github.com/rust-lang/rust/issues/40180">can be used in the IDT</a>.</p>

<p>If you want to write your own IDT management code from scratch, you can refer to the links related to interrupts in the section <a href="#recommended-reading-and-prerequisites">Recommended Reading and Prerequisites</a> as well as the <a href="https://docs.rs/x86_64/0.13.2/x86_64/">x86_64 crate source and documentation</a>.</p>

<h3 id="double-faults">Double Faults</h3>

<p>The double fault is one of the most pervasive exceptions an OS developer will encounter near the start of their project. Despite this, it’s actually one of the easiest to catch. When a Double Fault is invoked, that simply means that the CPU encountered a different exception and fails to invoke its corresponding handler. If it then fails to invoke the double fault’s handler, it causes a <em>triple fault</em> which has no handler and simply causes the CPU to reset itself.</p>

<p>Let’s get started implementing a basic Double Fault handler. For now, it’ll simply panic and log an error message and some relevant data to serial. Since it panics and therefore never returns, it will be marked as diverging (<code class="highlighter-rouge">-&gt; !</code>).</p>

<blockquote>
  <p>The <code class="highlighter-rouge">x86_64</code> crate implements <code class="highlighter-rouge">Debug</code> for the <code class="highlighter-rouge">InterruptStackFrame</code> struct, so we can effortlessly print the data within to Serial without much hassle.</p>
</blockquote>

<div class="language-rust highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="c">// src/interrupts.rs</span>
<span class="k">use</span> <span class="nn">x86_64</span><span class="p">::</span><span class="nn">structures</span><span class="p">::</span><span class="nn">idt</span><span class="p">::{</span><span class="n">InterruptDescriptorTable</span><span class="p">,</span> <span class="n">InterruptStackFrame</span><span class="p">};</span>
<span class="k">use</span> <span class="nn">lazy_static</span><span class="p">::</span><span class="n">lazy_static</span><span class="p">;</span>

<span class="nd">lazy_static!</span> <span class="p">{</span>
    <span class="k">static</span> <span class="k">ref</span> <span class="n">IDT</span><span class="p">:</span> <span class="n">InterruptDescriptorTable</span> <span class="o">=</span> <span class="p">{</span>
        <span class="k">let</span> <span class="k">mut</span> <span class="n">idt</span> <span class="o">=</span> <span class="nn">InterruptDescriptorTable</span><span class="p">::</span><span class="nf">new</span><span class="p">();</span>
        <span class="c">// Set Handler Funcs below</span>
        <span class="n">idt</span><span class="py">.double_fault</span><span class="nf">.set_handler_fn</span><span class="p">(</span><span class="n">double_fault</span><span class="p">);</span>

        <span class="n">idt</span>
    <span class="p">};</span>
<span class="p">}</span>

<span class="k">pub</span> <span class="k">fn</span> <span class="nf">init</span><span class="p">()</span> <span class="p">{</span>
    <span class="n">IDT</span><span class="nf">.load</span><span class="p">();</span>
<span class="p">}</span>

<span class="k">extern</span> <span class="s">"x86-interrupt"</span> <span class="k">fn</span> <span class="nf">double_fault</span><span class="p">(</span>
    <span class="n">stack_frame</span><span class="p">:</span> <span class="o">&amp;</span><span class="k">mut</span> <span class="n">InterruptStackFrame</span><span class="p">,</span> <span class="mi">_</span><span class="n">error_code</span><span class="p">:</span> <span class="nb">u64</span><span class="p">)</span> <span class="k">-&gt;</span> <span class="o">!</span>
<span class="p">{</span>
    <span class="nd">panic!</span><span class="p">(</span><span class="s">"EXCEPTION: DOUBLE FAULT</span><span class="se">\n</span><span class="s">{:#?}"</span><span class="p">,</span> <span class="n">stack_frame</span><span class="p">);</span>
<span class="p">}</span>
</code></pre></div></div>

<p>Now we can initialize the IDT and intentionally cause a page fault by writing to some unallocated memory. Since we have no page fault handler, our double fault handler should run. We shouldn’t forget to upgrade our panic function while we’re at it, since currently it simply hangs forever once called.</p>

<div class="language-rust highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="c">// src/lib.rs</span>

<span class="nd">#[no_mangle]</span>
<span class="k">pub</span> <span class="k">extern</span> <span class="s">"C"</span> <span class="k">fn</span> <span class="nf">rust_main</span><span class="p">()</span> <span class="k">-&gt;</span> <span class="o">!</span> <span class="p">{</span>
    <span class="nd">printsln!</span><span class="p">(</span><span class="s">"Hello, World!"</span><span class="p">);</span>

    <span class="nn">interrupts</span><span class="p">::</span><span class="nf">init</span><span class="p">();</span>

    <span class="c">// trigger a page fault</span>
    <span class="k">unsafe</span> <span class="p">{</span>
        <span class="o">*</span><span class="p">(</span><span class="mi">0xdeadbeef</span> <span class="k">as</span> <span class="o">*</span><span class="k">mut</span> <span class="nb">u64</span><span class="p">)</span> <span class="o">=</span> <span class="mi">42</span><span class="p">;</span>
    <span class="p">};</span>

    <span class="k">loop</span> <span class="p">{}</span>
<span class="p">}</span>

<span class="nd">#[panic_handler]</span>
<span class="k">fn</span> <span class="nf">panic</span><span class="p">(</span><span class="n">info</span><span class="p">:</span> <span class="o">&amp;</span><span class="n">PanicInfo</span><span class="p">)</span> <span class="k">-&gt;</span> <span class="o">!</span> <span class="p">{</span>
    <span class="nd">println!</span><span class="p">(</span><span class="s">"{}"</span><span class="p">,</span> <span class="n">info</span><span class="p">);</span>
    <span class="k">loop</span> <span class="p">{}</span>
<span class="p">}</span>
</code></pre></div></div>

<p>One <code class="highlighter-rouge">make run</code> later and we can see that our <code class="highlighter-rouge">serial.log</code> reflects our changes beautifully! It even displays the location in the source where the panic occurred, thanks to rust’s <code class="highlighter-rouge">PanicInfo</code> doing its magic.</p>

<p><img src="/img/osdev-double-fault-exception.png" alt="The Double Fault handler working as intended." /></p>

<h2 id="global-descriptor-table">Global Descriptor Table</h2>

<h3 id="stack-switching">Stack Switching</h3>

<p>If a stack overflow in the kernel leads to a double fault, then when the handler tries to push its own data onto the stack it will cause a triple fault. Fortunately, x86_64 CPUs are able to switch to a known-good stack whenever an exception handler is called, so this problem can never occur.</p>

<p>In order to implement this for the CPU, we need to make use of the <a href="https://wiki.osdev.org/Task_State_Segment">Task State Segment</a>. Not only will this allow us to fix our stack-sanity problems, but it will also come in handy for switching between user mode and kernel mode later on.</p>

<h2 id="source">Source</h2>

<p>The entire source for this OS is available on the project’s repository on GitHub: <a href="https://github.com/Montessquio/Rust-OS/tree/Post-2">https://github.com/Montessquio/Rust-OS/</a>.</p>

<h2 id="licensing">Licensing</h2>

<p>This text includes software written by Philipp Oppermann and released under the MIT License, which is reproduced in full below:</p>

<p>The MIT License (MIT)</p>

<p>Copyright (c) 2015 Philipp Oppermann</p>

<p>Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:</p>

<p>The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.</p>

<p>THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</p>
:ET